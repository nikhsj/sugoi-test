# README 
The app is meant as a bare bones prototype to check certain javascript functionality.
Ideally, it should be structured using classes to help readability. But as of now, it works and does what is required.

A working model can be viewed at [CodePen](http://codepen.io/anon/pen/advMQO).

Nothing has been done to prettify the output since it hadn't been specified.

Further features would be added on request.
