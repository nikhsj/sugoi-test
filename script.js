console.log("it begins...");

function init()
{
	cell_width = 120;
	cell_height = 100;
	current_score =0;
	grid = document.getElementById('grid');
	timer = document.getElementById('timer');
	score = document.getElementById('score')
	grid_size = 8
	setup();
}

// Start the game
function setup()
{

	current_wrong = 0;
	current_right = 0;
	total_wrong = 0;
	total_right = 0;
	// Set beginner level -- global
	if(!grid.hasAttribute('data-level')){
		grid.setAttribute('data-level', 1);
		update();
	}

}

// Setup clock and grid for the level
function update()
{
	// Update score
	total_right+= current_right;
	total_wrong+= current_wrong;
	setScore();
	current_wrong = 0;
	current_right = 0;

	var level;
	switch(grid.getAttribute('data-level')){
		case "1":
			level = 1; break;
		case "2":
			level = 2; break;
		case "3":
			level = 3; break;
		default:
			level = -1;break;
	}

	if(level > 0){
		buildGrid();
		// Bind duratiopn for level
		var duration;
		switch(level){
			case 1:
				duration = 60;break;
			case 2:
				duration = 30;break;
			case 3:
				duration = 15;break;
			default:
				duration = 60;break;
		}

		setTimer(duration);
		
	}else{
		showEnd();

	}

}

function setTimer(duration)
{
	var seconds = parseInt(duration,10); 

	interval = setInterval(function(){
		var display = (seconds<10)? "0"+seconds : seconds;
		timer.textContent = display;

		if(--seconds<0){
			// Show current level stats
			alert('Out of time! You got '+ current_wrong+ ' elements wrong and '+ current_right+ ' elements right.');

			// Prepare for next level
			current_level = parseInt(grid.getAttribute('data-level'),10);
			grid.setAttribute('data-level', current_level+1)
			clearInterval(interval);
			update();
		}

	}, 1000);

}

// Set the score to div to current score after each level
function setScore()
{
	score.textContent = current_score;
}

// Helper function to show endscreen
function showEnd()
{
		// Clear the grid 
	while(grid.firstChild){
		grid.removeChild(grid.firstChild);
	}
	div = document.createElement('div');
	div.textContent = "THE END.Your score is "+ current_score+'. Total wrong: '+total_wrong+'. Total right: '+total_right;

	grid.appendChild(div);
}

// Build the grid
function buildGrid()
{
	// clear previous grid
	while(grid.firstChild){
		grid.removeChild(grid.firstChild);
	}

	// create grid 
	var squareCount = 10;
	var n = grid_size;
	n = (n<4)?4:n;
	for(var i=0;i<n;i++){
		var div = document.createElement('div');
		div.setAttribute('class','row-'+i);
		for(var j=0;j<n;j++){
			var span = document.createElement('span');
			span.setAttribute('class','col-'+j);
			id = 'r'+i+'c'+j;
			span.setAttribute('id', id);

			// span.textContent ='('+ i+','+j +')';

			var canvas = document.createElement('canvas');
			canvas.setAttribute('id', i+'-'+j);
			canvas.setAttribute('width', cell_width);
			canvas.setAttribute('height', cell_height);
			canvas.setAttribute('class', 'clickable');

			canvas.addEventListener('mouseover', addBg )
			canvas.addEventListener('mouseout', removeBg )
			canvas.addEventListener('click', isSquare )
	    // canvas.setAttribute('style','border:1px solid #000000;');

	    var rnd = Math.random();
	    if(squareCount>0 && rnd<=0.6){
	    	square(canvas);
	    	squareCount--;
	    }else{
	    	if(rnd<0.5)
	    		triangle(canvas);
	    	else
	    		rectangle(canvas);
	    	
	    }

			span.appendChild(canvas);
			div.appendChild(span);

		}
		grid.appendChild(div);
	}

}

// Highlight cell when hovering
function addBg()
{
	this.setAttribute('style','background-color:rgba(232, 186, 0,0.5)');
}

// Remove highlight on mouse move
function removeBg()
{
	this.removeAttribute('style');
}

// Checks if the cell is a square
function isSquare()
{
	this.removeAttribute('class');
	this.removeEventListener('mouseover');
	this.removeEventListener('mouseout');

	if(this.getAttribute('data-correct') == "true"){
		showCorrect(this);
		// Add 10 points to score.
		current_score += 10; 
		current_right+=1;
	}else{
		showWrong(this);
		// Deduct 5 points on wrong answer.
		current_score -= 5;
		current_wrong+=1;
	}

// Remove cells click property.
	this.removeEventListener('click', isSquare);
}

// show highlight for correct answer
function showCorrect(canvas)
{
	context = canvas.getContext('2d');
	context.globalAlpha = 0.5;
  	context.fillStyle = "green";
  	context.fillRect(0,0,canvas.width, canvas.height);
  	context.globalAlpha = 1;
}

// show highlight for wrong answer
function showWrong(canvas)
{
	context = canvas.getContext('2d');
	context.globalAlpha = 0.5;
  	context.fillStyle = "red";
  	context.fillRect(0,0,canvas.width, canvas.height);
  	context.globalAlpha = 1;

}

// Draw square
function square(canvas)
{

  	var dx = canvas.width/10;
  	var dy = canvas.height/10;
	var a;
	if(canvas.width<=canvas.height)
		a = canvas.width -dx;
	else
		a= canvas.height -dy;  	
	canvas.setAttribute('data-correct',true);

	context = canvas.getContext('2d');
  	context.fillStyle = "grey";
  	context.fillRect(dx/2, dy/2, a, a);
  	context.fillStyle = "white";
  	context.fillText("Square",a/2,a/2);
  
}

// Draw triangle
function triangle(canvas)
{
  	var h = canvas.height;
  	var w = canvas.width;
  	var dx = canvas.width/10;
  	var dy = canvas.height/10;

  	canvas.setAttribute('data-correct',false);

	context = canvas.getContext('2d');
  	context.fillStyle = "grey";
  	context.beginPath();
  	context.moveTo(dx,h-dy);
  	context.lineTo(w-dx, h-dy);
  	context.lineTo(w/2 , dy );
  	context.lineTo(dx,h-dy);
  	context.closePath();
  	context.fill();
}

// Draw rectangle
function rectangle(canvas)
{
	var h = canvas.height;
  	var w = canvas.width;
  	var dx = canvas.width/20;
  	var dy = canvas.height/10;

  	canvas.setAttribute('data-correct',false);

	context = canvas.getContext('2d');
  	context.fillStyle = "grey";
  	context.fillRect(dx/2,dy/2, w-dx, h-dy);
  
}